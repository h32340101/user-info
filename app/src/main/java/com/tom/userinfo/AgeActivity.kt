package com.tom.userinfo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.tom.userinfo.MainActivity.Companion.PREF
import kotlinx.android.synthetic.main.activity_age.*
import kotlinx.android.synthetic.main.activity_age.nextButton

class AgeActivity : AppCompatActivity() {
    private val sensitiveData = SensitiveData.Instance ?: SensitiveData()
    private val encryptedSharedPreferences by lazy {
        getSharedPreferences(MainActivity.PREF, MODE_PRIVATE)
//        val masterKey = MasterKey.Builder(this)
//            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
//            .build()
//        EncryptedSharedPreferences.create(
//            this,
//            PREF,
//            masterKey,
//            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
//            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
//        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_age)

        sensitiveData.age?.also { ageEditText.setText(it.toString()) }

        nextButton.setOnClickListener {
            sensitiveData.age =
                ageEditText.text.toString().toIntOrNull()
            val intent = Intent(this, GenderActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStop() {
        super.onStop()
        val data = SensitiveData.Instance
        val json2 = SensitiveDataSerializer().toJson(sensitiveData)
        encryptedSharedPreferences.edit()
            .putString("sensitiveDataPending", json2)
            .apply()
        Log.d(TAG, "onDestroy: $data. 存檔")
        Log.d(TAG, "onDestroy: $json2. 存檔")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(AgeActivity::class.java.simpleName, "結束")
    }

    companion object {
        private const val TAG = "AgeActivity"
    }
}