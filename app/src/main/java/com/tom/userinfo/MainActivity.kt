package com.tom.userinfo

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var userInfo: SensitiveData? = null

    private val encryptedSharedPreferences by lazy {
        getSharedPreferences(PREF, MODE_PRIVATE)
//        val masterKey = MasterKey.Builder(this)
//            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
//            .build()
//        EncryptedSharedPreferences.create(
//            this,
//            PREF,
//            masterKey,
//            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
//            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
//        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "產生")
        intent.getBooleanExtra("userConfirmChange", false).let { applyChange ->
            if (applyChange) {
                Log.d(TAG, "使用者編輯完畢, 寫入 xml, 清暫存")
                userInfo = SensitiveData.Instance?.copy()
                saveUserInfo()
            }
            else {
                userInfo = encryptedSharedPreferences
                    .getString("sensitiveData", null)?.let {
                        SensitiveDataSerializer().fromJson(it)
                    } ?: SensitiveData.Instance?.copy()
            }
        }

        userInfo?.run {
            nickName?.also { nickNameTextView.text = it }
            age?.also { ageTextView.text = it.toString() }
            gender?.also { genderTextView.text = it.toString() }
        }

        val str = encryptedSharedPreferences.getString("sensitiveDataPending", null)
        val userInfoPending = str?.let {
            Log.d(TAG, "$it. 讀未完成的暫存")
            SensitiveDataSerializer().fromJson(it)
        }
        userInfoPending?.let {
            SensitiveData.Instance = it
            AlertDialog.Builder(this)
                .setTitle("編輯")
                .setMessage("編輯尚未完成")
                .setNeutralButton("ok", null)
                .show()
        }

        SensitiveData.Instance?.run {
            nickName?.also { debugNickNameTextView.text = it }
            age?.also { debugAgeTextView.text = it.toString() }
            gender?.also { debugGenderTextView.text = it.toString() }
        }

        editUserInfoButton.setOnClickListener(intentUserInfoEditor)
        appSettingsButton.setOnClickListener(intentAppSettings)
    }

    private val intentUserInfoEditor = View.OnClickListener {
        if (SensitiveData.Instance == null)
            SensitiveData.Instance = SensitiveData()
        val intent = Intent(this, NicknameActivity::class.java)
        startActivity(intent)
    }

    private val intentAppSettings = View.OnClickListener {
        val intent = Intent().apply {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.parse("package:" + application.packageName)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        startActivity(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        encryptedSharedPreferences.edit()
            .remove("sensitiveDataPending")
            .apply()
    }

    @SuppressLint("ApplySharedPref") // SharedPreference 使用 commit
    private fun saveUserInfo() {
        val data = SensitiveData.Instance
        val json = SensitiveDataSerializer().toJson(data)
        encryptedSharedPreferences.edit()
            .putString("sensitiveData", json)
            .remove("sensitiveDataPending")
            .apply()
        SensitiveData.Instance = null
    }

    companion object {
        private const val TAG = "MainActivity"
        const val PREF = "shared_prefs"
    }
}