package com.tom.userinfo

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException
import java.lang.NullPointerException

class SensitiveDataSerializer {
    fun fromJson(json: String?): SensitiveData? {
        return try {
            val jsonElement = JsonParser.parseString(json)
            Gson().fromJson(jsonElement, SensitiveData::class.java)
        } catch (ex: JsonSyntaxException) {
            Log.d(TAG, "JsonSyntaxException: JsonParser.parseString(json)")
            null
        } catch (ex: NullPointerException) {
            Log.d(TAG, "NullPointerException: JsonParser.parseString(json)")
            null
        }
    }

    fun toJson(sensitiveData: SensitiveData?): String {
        val gson = GsonBuilder().apply {
            serializeNulls()
        }.create()
        return gson.toJson(sensitiveData, SensitiveData::class.java)
    }

    companion object {
        private const val TAG = "SensitiveDataSerializer"
    }
}

data class SensitiveData(
    var nickName: String? = null,
    var age: Int? = null,
    var gender: Int? = null
) {
    companion object {
//        private val SingletonInstance by lazy { SensitiveData() }
//        fun getInstance() = SingletonInstance
        var Instance: SensitiveData? = null
    }
}