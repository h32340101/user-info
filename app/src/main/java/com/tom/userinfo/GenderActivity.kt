package com.tom.userinfo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.view.forEach
import androidx.core.view.get
import androidx.core.view.size
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.tom.userinfo.MainActivity.Companion.PREF
import kotlinx.android.synthetic.main.activity_gender.*

class GenderActivity : AppCompatActivity() {
    private val sensitiveData = SensitiveData.Instance ?: SensitiveData()
    private val encryptedSharedPreferences by lazy {
        getSharedPreferences(MainActivity.PREF, MODE_PRIVATE)
//        val masterKey = MasterKey.Builder(this)
//            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
//            .build()
//        EncryptedSharedPreferences.create(
//            this,
//            PREF,
//            masterKey,
//            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
//            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
//        )
    }
    private val genders by lazy { // arrayOf("female", "male", "transgender")
        Array(genderRadioGroup.childCount) { i -> (genderRadioGroup[i] as RadioButton).text }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gender)

        sensitiveData.gender?.also {
            val id = genderRadioGroup.getChildAt(it)?.id
                ?: -1
            genderRadioGroup.check(id)
        }
        nextButton.setOnClickListener {
            val text = findViewById<RadioButton>(genderRadioGroup.checkedRadioButtonId)?.text
            val gender = genders.indexOf(text).let {
                if (it < 0)
                    -1
                else
                    it
            }
            sensitiveData.gender = gender
            val intent = Intent(this, MainActivity::class.java).apply {
                flags = flags
                    .or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .or(Intent.FLAG_ACTIVITY_NEW_TASK)
                putExtra("userConfirmChange", true)
            }
            isPressNext = true
            startActivity(intent)
        }
    }

    private var isPressNext = false
    override fun onStop() {
        super.onStop()
        val data = SensitiveData.Instance
        val json2 = SensitiveDataSerializer().toJson(sensitiveData)
        encryptedSharedPreferences.edit().apply {
            // 實測 MainActivity 在 back stack 時, 如果清空 back stack 之後再啟動 MainActivity
            // 在 onCreate 馬上執行刪除 SharedPreferences 項目會無效
            // 所以先在啟動 MainActivity 之前刪除暫存
            if (isPressNext)
                remove("sensitiveDataPending")
            else
            putString("sensitiveDataPending", json2)
        }.apply()
        Log.d(TAG, "onDestroy: $data. 存檔")
        Log.d(TAG, "onDestroy: $json2. 存檔")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(GenderActivity::class.java.simpleName, "結束")

    }

    companion object {
        private const val TAG = "GenderActivity"
    }
}